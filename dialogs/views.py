from django.shortcuts import render
from django.views.generic import View

from rest_framework import generics
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from accounts.models import CustomUser
from dialogs.models import Thread, Message
from dialogs.serializers import ThreadSerializer, MessageSerializer


class MainPageView(View):
    """ Show Main Page (index.html) """

    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class ThreadList(APIView):
    """
    Create thread and get all threads.
    URL: http://localhost:8000/threads/
    Allowed methods: GET, POST
    """

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request):
        threads = Thread.objects.all()
        serializer = ThreadSerializer(threads, many=True)
        return Response(serializer.data)

    def post(self, request):
        current_user = request.user
        if current_user.user_type == CustomUser.USER_TYPE_ADMIN:
            serializer = ThreadSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Only admin can create thread!'}, status=status.HTTP_400_BAD_REQUEST)


class ThreadDetail(APIView):
    """
    Get thread, update thread, delete thread.
    URL: http://localhost:8000/threads/<id>/
    Allowed methods: GET, PUT, DELETE
    """

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get(self, request, pk):
        try:
            thread = Thread.objects.get(pk=pk)
            serializer = ThreadSerializer(thread)
            return Response(serializer.data)
        except Exception:
            return Response({'detail': 'Thread with id {} does not exist!'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        try:
            thread = Thread.objects.get(pk=pk)
            serializer = ThreadSerializer(thread, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception:
            return Response({'detail': 'Thread with id {} does not exist!'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            thread = Thread.objects.get(pk=pk)
        except Exception:
            return Response({'detail': 'Thread with id {} does not exist!'.format(pk)},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            current_user = request.user
            if current_user.user_type == CustomUser.USER_TYPE_ADMIN:
                thread.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response({'detail': 'Only admin can delete thread!'.format(pk)},
                                status=status.HTTP_400_BAD_REQUEST)


class MessageList(generics.ListCreateAPIView):
    """
    Create message and get all messages.
    URL: http://localhost:8000/messages/
    Allowed methods: GET, POST
    """

    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class MessageDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get message, update message, delete message.
    URL: http://localhost:8000/messages/<id>/
    Allowed methods: GET, PUT, DELETE
    """

    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ThreadMessageList(APIView):
    """
    Get all messages from thread, add message to thread.
    URL: http://localhost:8000/threads/<thread_id>/messages/<id>/
    Allowed methods: GET, POST
    """

    def get(self, request, pk):
        thread = Thread.objects.get(pk=pk)
        messages = Message.objects.filter(thread=thread)
        serializer = MessageSerializer(messages, many=True)
        return Response(serializer.data)

    def post(self, request,  pk):
        request.data['thread'] = pk
        serializer = MessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ThreadUserAddDelete(APIView):
    """
    Add user to thread, delete user from thread (and delete thread if no drivers in thread)
    URL: http://localhost:8000/threads/<thread_id>/users/<user_id>/
    Allowed methods: POST, DELETE
    """

    def post(self, request, thread_pk, user_pk):
        current_user = request.user
        if current_user.user_type == CustomUser.USER_TYPE_ADMIN:
            try:
                user = CustomUser.objects.get(pk=user_pk)
                thread = Thread.objects.get(pk=thread_pk)
            except Exception:
                return Response({'detail': 'The Thread or user is not exists!'},
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                if user.user_type == CustomUser.USER_TYPE_ADMIN \
                        and thread.participants.filter(user_type=CustomUser.USER_TYPE_ADMIN).count() > 0:
                    return Response({'detail': 'Admin already exists in this thread!'},
                                    status=status.HTTP_400_BAD_REQUEST)
                else:
                    thread.participants.add(user)
                    serializer = ThreadSerializer(thread, data={'participants': [participant.id for participant in
                                                                                 thread.participants.all()]})
                    if serializer.is_valid():
                        serializer.save()
                        return Response(serializer.data)

                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Only Admin can add participants to thread!'},
                            status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, thread_pk, user_pk):
        current_user = request.user
        if current_user.user_type == CustomUser.USER_TYPE_ADMIN:
            try:
                user_to_delete = CustomUser.objects.get(pk=user_pk)
                thread = Thread.objects.get(pk=thread_pk)
            except Exception:
                return Response({'detail': 'The Thread or user is not exists!'}, status=status.HTTP_400_BAD_REQUEST)

            thread.participants.remove(user_to_delete)
            if CustomUser.USER_TYPE_DRIVER in [participant.user_type for participant in thread.participants.all()]:
                serializer = ThreadSerializer(thread, data={'participants':
                                                        [participant.id for participant in thread.participants.all()]})
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                thread.delete()
                return Response({'detail': 'The Thread was deleted because there were no Drivers left!'},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'Only Admin can delete participants from thread!'},
                            status=status.HTTP_400_BAD_REQUEST)

