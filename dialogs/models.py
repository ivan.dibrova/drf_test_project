from django.db import models

from accounts.models import CustomUser


class Thread(models.Model):
    participants = models.ManyToManyField(CustomUser, related_name='participants')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created',)


class Message(models.Model):
    text = models.CharField(max_length=512)
    sender = models.ForeignKey(CustomUser, related_name='sender', on_delete=models.CASCADE, unique=False)
    thread = models.ForeignKey(Thread, related_name='thread', on_delete=models.CASCADE, unique=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)

