from django.contrib import admin

from dialogs.models import Thread, Message


@admin.register(Thread)
class ThreadAdmin(admin.ModelAdmin):
    fields = ['participants']
    list_display = ('participants_list', 'messages_list', 'created', 'updated')

    def participants_list(self, obj):
        return ' '.join([p.username for p in obj.participants.all()])

    def messages_list(self, obj):
        messages = Message.objects.filter(thread=obj)
        messages_list = []
        for message in messages:
            messages_list.append('{}:"{}"'.format(message.sender.username, message.text))
        return ' '.join(messages_list)


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('text', 'sender', 'thread')
