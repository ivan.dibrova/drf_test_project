from rest_framework import serializers

from accounts.models import CustomUser
from dialogs.models import Thread, Message


class ThreadSerializer(serializers.ModelSerializer):
    participants = serializers.PrimaryKeyRelatedField(many=True, queryset=CustomUser.objects.all())

    class Meta:
        model = Thread
        fields = ('id', 'participants', 'created', 'updated')


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.PrimaryKeyRelatedField(many=False, queryset=CustomUser.objects.all())
    thread = serializers.PrimaryKeyRelatedField(many=False, queryset=Thread.objects.all())

    class Meta:
        model = Message
        fields = ('id', 'text', 'sender', 'thread', 'created')

