from django.urls import path
from dialogs.views import MainPageView, ThreadList, ThreadDetail, MessageList, MessageDetail, ThreadMessageList, \
    ThreadUserAddDelete


urlpatterns = [
    path('', MainPageView.as_view(), name='main_page'),

    # GET, POST
    path('threads/', ThreadList.as_view()),
    # GET, PUT, DELETE
    path('threads/<int:pk>/', ThreadDetail.as_view()),

    # GET, POST
    path('messages/', MessageList.as_view()),
    # GET, PUT, DELETE
    path('messages/<int:pk>/', MessageDetail.as_view()),

    # GET, POST
    path('threads/<int:pk>/messages/', ThreadMessageList.as_view()),
    # POST, DELETE
    path('threads/<int:thread_pk>/users/<int:user_pk>/', ThreadUserAddDelete.as_view()),
]

