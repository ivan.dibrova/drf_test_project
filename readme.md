Test project.

1. Сделать application "accounts".
    Создать модель CustomUser со след. полями:
        - username
        - first_name
        - last_name
        - email
        - password
        - user_type ("Admin", "Driver')

2. Реализовать возможность добавления user'ов через команду с созданием JWT Token'a.

3. Сделать application "dialogs" c моделями:
    Thread:
        - participants
        - created (date)
        - updated (date)
    Message:
        - text
        - sender
        - thread
        - created

4. API urls для:
    - создания Message для Participant'ов Thread'a
    - получения списка Thread'ов юзера
    - получения сообщений из Thread'a
    - добавления/удаления Driver'а из Thread'a(удалять может только Admin, если в Thread'e не остается никого кроме Admin'a - удалять сам Thread)
------------------------------------------------------------------------------------------------------------------------

Install project (Ubuntu)

1. sudo apt-get update
2. sudo apt-get install python3.6
3. sudo apt-get install virtualenv
4. git clone git@gitlab.com:ivan.dibrova/drf_test_project.git
5. cd drf_test_project
6. python3.6 -m virtualenv venv
7. cd venv/bin
8. source activate
9. cd ../..
10. pip3 install --user --upgrade setuptools
11. pip3 install --user -r requirements.txt
12. python3.6 manage.py makemigrations
13. python3.6 manage.py migrate
14. python3.6 manage.py runserver
----- Create user and get access token (in another Terminal) -----
15. python manage.py createcustomuser -u admin -p asd12345 -e admin@example.com -f Admin_firstname -l Admin_lastname -t 1


------------------------------------------------------------------------------------------------------------------------

***** Commands Virtualenv environment *****

Добавление user'ов через команду с созданием JWT Token'a.

Create user and get token (example):

    python manage.py createcustomuser -u admin -p asd12345 -e admin@example.com -f Admin_firstname -l Admin_lastname -t 1

Get token for existing user (example):

    python manage.py getusertoken -u admin -p asd12345

------------------------------------------------------------------------------------------------------------------------

***** Request examples using httpie *****

API urls для получения списка Thread'ов юзера

Get thread list for user 12:

    http GET http://localhost:8000/users/12/threads/ "Authorization: Bearer <access_token>"

API urls для создания Message для Participant'ов Thread'a

Create message for thread with id 7:

    http POST http://localhost:8000/threads/7/messages/ text="Message_71" sender=1 "Authorization: Bearer <access_token>"

API urls для получения сообщений из Thread'a

Get messages for thread with id 7:

    http GET http://localhost:8000/threads/7/messages/ "Authorization: Bearer <access_token>"

API urls для добавления/удаления Driver'а из Thread'a(удалять может только Admin, если в Thread'e не остается никого кроме Admin'a - удалять сам Thread)

Add user with id 31 to thread with id 7:

    http POST http://localhost:8000/threads/7/users/31/ "Authorization: Bearer <access_token>"

Delete user with id 34 from thread with id 7:

    http DELETE http://localhost:8000/threads/7/users/34/ "Authorization: Bearer <access_token>"



Other requests:

Get token for existing user:

    http POST http://localhost:8000/token/ username="admin" password="asd12345"

Create user:

    http POST http://localhost:8000/users/ username="driver_1" password="asd12345" email="driver_1@example.com" user_type=2 "Authorization: Bearer <access_token>"

Get all users list:

    http GET http://localhost:8000/users/ "Authorization: Bearer <access_token>"

Get one user with id 12:

    http GET http://localhost:8000/users/12/ "Authorization: Bearer <access_token>"

Update user with id 12:

    http PUT http://localhost:8000/users/12/ username="driver_12_1" password="asd12345" email="new_email@example.com" user_type=2 "Authorization: Bearer <access_token>"

Delete user with id 12:

    http DELETE http://localhost:8000/users/12/ "Authorization: Bearer <access_token>"

Create thread:

    http POST http://localhost:8000/threads/ participants:="[1, 2, 5, 19, 39]" "Authorization: Bearer <access_token>"

Get all threads list:

    http GET http://localhost:8000/threads/ "Authorization: Bearer <access_token>"

Get thread with id 2:

    http GET http://localhost:8000/threads/2/ "Authorization: Bearer <access_token>"

Update thread with id 2:

    http PUT http://localhost:8000/threads/2/ participants:="[3, 7, 31, 32, 45]" user_type=2 "Authorization: Bearer <access_token>"
    
Delete thread with id 2:

    http DELETE http://localhost:8000/threads/2/ "Authorization: Bearer <access_token>"

Create message:

    http POST http://localhost:8000/messages/ text="Message_21" sender=1 thread=7 "Authorization: Bearer <access_token>"

Get all messages list:

    http GET http://localhost:8000/messages/ "Authorization: Bearer <access_token>"

Get message with id 5:

    http GET http://localhost:8000/messages/5/ "Authorization: Bearer <access_token>"

Update message with id 5:

    http PUT http://localhost:8000/messages/5/ text="Message_5_1" sender=31 thread=7 "Authorization: Bearer <access_token>"

Delete message with id 5:

    http DELETE http://localhost:8000/messages/5/ "Authorization: Bearer <access_token>"

------------------------------------------------------------------------------------------------------------------------

***** Request examples using APIRequests from accounts/api_requests.py *****

API urls для получения списка Thread'ов юзера

Get thread list for user 12:

    APIRequests.get_user_threads(access_token, user_id='12')

API urls для создания Message для Participant'ов Thread'a

Create message for thread with id 7:

    APIRequests.create_message_for_thread(access_token, thread_id=7, text='Message_71', sender=1)

API urls для получения сообщений из Thread'a

Get messages for thread with id 7:

    APIRequests.get_all_messages_for_thread(access_token, thread_id=7)

API urls для добавления/удаления Driver'а из Thread'a(удалять может только Admin, если в Thread'e не остается никого кроме Admin'a - удалять сам Thread)

Add user with id 31 to thread with id 7:

    APIRequests.add_user_to_thread(access_token, thread_id=7, user_id=31)

Delete user with id 34 from thread with id 7:

    APIRequests.delete_user_from_thread(access_token, thread_id=7, user_id=34)



Other requests:

Get token for existing user:

    APIRequests.get_user_access_token(user)

Create user:

    APIRequests.create_user('driver_1', 'asd12345', access_token, email='driver_1@example.com' user_type=CustomUser.USER_TYPE_DRIVER)

Get all users list:

    APIRequests.get_all_users(access_token)

Get one user with id 12:

    APIRequests.get_one_user(access_token, username='driver_12')

        or

    APIRequests.get_one_user(access_token, user_id=12)

Update user with id 12:

    APIRequests.update_user(access_token, user_id='12', username='driver_12_1', password='asd12345', email="new_email@example.com", user_type=CustomUser.USER_TYPE_DRIVER)

Delete user with id 12:

    APIRequests.delete_user(access_token, username='driver_12')

        or

    APIRequests.delete_user(access_token, user_id=12)

Create thread:

    APIRequests.create_thread(access_token, participants=[1, 2, 5, 19, 39])

Get all threads list:

    APIRequests.get_all_threads(access_token)

Get thread with id 2:

    APIRequests.get_one_thread(access_token, thread_id=2)
    
Update thread with id 2:

    APIRequests.update_thread(access_token, thread_id=2, participants=[3, 7, 31, 32, 45])

Delete thread with id 2:

    APIRequests.delete_thread(access_token, user_id=2)

Create message:

    APIRequests.create_message(access_token, text="Message_21" sender=1, thread=7)

Get all messages list:

    APIRequests.get_all_messages(access_token)

Get message with id 5:

    APIRequests.get_one_message(access_token, message_id=5)

Update message with id 5:

    APIRequests.update_message(access_token, message_id=5, text="Message_7_3" sender=31, thread=7)

Delete message with id 5:

    APIRequests.delete_message(access_token, message_id=5)

------------------------------------------------------------------------------------------------------------------------