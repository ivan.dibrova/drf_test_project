import json
import logging
import requests

from rest_framework_simplejwt.tokens import AccessToken

from accounts.models import CustomUser

logger_console = logging.getLogger('console')

API_URL_USERS_CREATE_GET_ALL = 'http://localhost:8000/users/'
API_URL_USERS_GET_PUT_DELETE = 'http://localhost:8000/users/{}/'

API_URL_USERS_GET_THREADS = 'http://localhost:8000/users/{}/threads/'

API_URL_THREADS_CREATE_GET_ALL = 'http://localhost:8000/threads/'
API_URL_THREADS_GET_PUT_DELETE = 'http://localhost:8000/threads/{}/'

API_URL_MESSAGES_CREATE_GET_ALL = 'http://localhost:8000/messages/'
API_URL_MESSAGES_GET_PUT_DELETE = 'http://localhost:8000/messages/{}/'

API_URL_THREADS_GET_ADD_MESSAGE = 'http://localhost:8000/threads/{}/messages/'
API_URL_THREADS_ADD_DELETE_USER = 'http://localhost:8000/threads/{}/users/{}/'


class APIRequests():
    """ Test API requests """

    @staticmethod
    def get_user_access_token(user):
        return str(AccessToken.for_user(user))

    @staticmethod
    def create_headers(token):
        return {
            'content-type': 'application/json',
            'Authorization': 'Bearer {}'.format(token),
        }

    @staticmethod
    def create_user(username, password, token, email='', firstname='', lastname='', user_type=None):
        """
        URL: http://localhost:8000/users/
        Method: POST
        """

        if username and password:
            try:
                if not user_type or not user_type in [CustomUser.USER_TYPE_ADMIN, CustomUser.USER_TYPE_DRIVER]:
                    user_type = CustomUser.USER_TYPE_ADMIN

                headers = APIRequests.create_headers(token)
                data = json.dumps({'username': username,
                                   'password': password,
                                   'email': email,
                                   'firstname': firstname,
                                   'lastname': lastname,
                                   'user_type': user_type})

                session = requests.Session()
                request = session.post(API_URL_USERS_CREATE_GET_ALL, headers=headers, data=data)
                logger_console.info('Create user {}: {}!\n'
                                    'Status code = {}!'.format(username, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error creating user {}: {}!'.format(username, exc))
        else:
            logger_console.info('User can\'t be created without username and password!')

    @staticmethod
    def get_all_users(token):
        """
        URL: http://localhost:8000/users/
        Method: GET
        """

        headers = APIRequests.create_headers(token)

        session = requests.Session()
        request = session.get(API_URL_USERS_CREATE_GET_ALL, headers=headers)
        logger_console.info('Get users list: {}!\nStatus code = {}!'.format(request.text, request.status_code))
        session.close()

    @staticmethod
    def get_user(user_id, username):
        if user_id:
            return CustomUser.objects.get(id=user_id)
        else:
            return CustomUser.objects.get(username=username)

    @staticmethod
    def get_one_user(token, user_id=None, username=None):
        """
        URL: http://localhost:8000/users/<id>/
        Method: GET
        """

        if user_id or username:
            try:
                user = APIRequests.get_user(user_id, username)
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.get(API_URL_USERS_GET_PUT_DELETE.format(user.id), headers=headers)
                logger_console.info('Get one user {} (id = {}): {}!\n'
                                    'Status code = {}!'.format(user, user.id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error getting user: {}!'.format(exc))
        else:
            logger_console.info('Username or user id are not set!')

    @staticmethod
    def update_user(token, user_id=None, username=None, password=None, email=None, first_name=None,
                    lastname=None, user_type=None):
        """
        URL: http://localhost:8000/users/<id>/
        Method: PUT
        """

        if user_id or username:
            try:
                headers = APIRequests.create_headers(token)
                user = APIRequests.get_user(user_id, username)
                data = {'id': user.id}

                if username:
                    data['username'] = username
                if password:
                    data['password'] = password
                if email:
                    data['email'] = email
                if first_name:
                    data['first_name'] = first_name
                if lastname:
                    data['email'] = lastname
                if user_type and user_type in [CustomUser.USER_TYPE_ADMIN, CustomUser.USER_TYPE_DRIVER]:
                    data['user_type'] = user_type

                data = json.dumps(data)

                session = requests.Session()
                request = session.put(API_URL_USERS_GET_PUT_DELETE.format(user.id), headers=headers, data=data)
                logger_console.info('Update user {} (id = {}): {}! '
                                    'Status code = {}!'.format(user, user.id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error updating user: {}!'.format(exc))
        else:
            logger_console.info('Username or user id are not set!')

    @staticmethod
    def delete_user(token, user_id=None, username=None):
        """
        URL: http://localhost:8000/users/<id>/
        Method: DELETE
        """

        if user_id or username:
            try:
                user = APIRequests.get_user(user_id, username)
                headers = APIRequests.create_headers(token)
                data = json.dumps({'id': user.id})

                session = requests.Session()
                request = session.delete(API_URL_USERS_GET_PUT_DELETE.format(user.id), headers=headers, data=data)
                logger_console.info('User {} (id = {}) was deleted! '
                                    'Status code = {}!'.format(user, user.id, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error deleting user: {}!'.format(exc))
        else:
            logger_console.info('Username or user id are not set!')

    @staticmethod
    def get_user_threads(token, user_id=None, username=None):
        """
        URL: http://localhost:8000/users/<user_id>/threads/
        Method: GET
        """

        if user_id or username:
            try:
                user = APIRequests.get_user(user_id, username)
                headers = APIRequests.create_headers(token)
                data = json.dumps({'id': user.id})

                session = requests.Session()
                request = session.get(API_URL_USERS_GET_THREADS.format(user.id), headers=headers, data=data)
                logger_console.info('User {} (id = {}) threads: {}! '
                                    'Status code = {}!'.format(user, user.id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error getting threads for user: {}!'.format(exc))
        else:
            logger_console.info('Username or user id are not set!')

    @staticmethod
    def create_thread(token, participants=None):
        """
        URL: http://localhost:8000/threads/
        Method: POST
        """

        if participants:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'participants': participants})

                session = requests.Session()
                request = session.post(API_URL_THREADS_CREATE_GET_ALL, headers=headers, data=data)
                logger_console.info('Thread created: {}!\nStatus code = {}!'.format(request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error creating thread: {}!'.format(exc))
        else:
            logger_console.info('Thread can\'t be created without participants!')

    @staticmethod
    def get_all_threads(token):
        """
        URL: http://localhost:8000/threads/
        Method: GET
        """

        headers = APIRequests.create_headers(token)

        session = requests.Session()
        request = session.get(API_URL_THREADS_CREATE_GET_ALL, headers=headers)
        logger_console.info('Get threads list: {}!\nStatus code = {}!'.format(request.text, request.status_code))
        session.close()

    @staticmethod
    def get_one_thread(token, thread_id=None):
        """
        URL: http://localhost:8000/threads/<id>/
        Method: GET
        """

        if thread_id:
            try:
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.get(API_URL_THREADS_GET_PUT_DELETE.format(thread_id), headers=headers)
                logger_console.info('Get one thread with id {}: {}!\n'
                                    'Status code = {}!'.format(thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error getting thread with id {}: {}!'.format(thread_id, exc))
        else:
            logger_console.info('Thread id is not set!')

    def update_thread(token, thread_id=None, participants=None):
        """
        URL: http://localhost:8000/threads/<id>/
        Method: PUT
        """

        if thread_id and participants:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'participants': participants})

                session = requests.Session()
                request = session.put(API_URL_THREADS_GET_PUT_DELETE.format(thread_id), headers=headers, data=data)
                logger_console.info('Update thread with id {}: {}!\n'
                                    'Status code = {}!'.format(thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error updating thread with id {}: {}!'.format(thread_id, exc))
        else:
            logger_console.info('Thread id or participants are not set!')

    @staticmethod
    def delete_thread(token, thread_id=None):
        """
        URL: http://localhost:8000/threads/<id>/
        Method: DELETE
        """

        if thread_id:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'id': thread_id})

                session = requests.Session()
                request = session.delete(API_URL_THREADS_GET_PUT_DELETE.format(thread_id), headers=headers, data=data)
                logger_console.info('Thread with id {} was deleted!\n'
                                    'Status code = {}!'.format(thread_id, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error deleting thread with id {}: {}!'.format(thread_id, exc))
        else:
            logger_console.info('Thread id is not set!')

    @staticmethod
    def create_message(token, text=None, sender=None, thread=None):
        """
        URL: http://localhost:8000/messages/
        Method: POST
        """

        if text and sender and thread:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'text': text, 'sender': sender, 'thread': thread})

                session = requests.Session()
                request = session.post(API_URL_MESSAGES_CREATE_GET_ALL, headers=headers, data=data)
                logger_console.info('Create message: {}!\nStatus code = {}!'.format(request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error creating message: {}!'.format(exc))
        else:
            logger_console.info('Message can\'t be created without text, sender and thread!')

    @staticmethod
    def get_all_messages(token):
        """
        URL: http://localhost:8000/messages/
        Method: GET
        """

        headers = APIRequests.create_headers(token)

        session = requests.Session()
        request = session.get(API_URL_MESSAGES_CREATE_GET_ALL, headers=headers)
        logger_console.info('Get messages list: {}!\nStatus code = {}!'.format(request.text, request.status_code))
        session.close()

    @staticmethod
    def get_one_message(token, message_id=None):
        """
        URL: http://localhost:8000/messages/<id>/
        Method: GET
        """

        if message_id:
            try:
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.get(API_URL_MESSAGES_GET_PUT_DELETE.format(message_id), headers=headers)
                logger_console.info('Get one message with id {}: {}!\n'
                                    'Status code = {}!'.format(message_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error getting message with id {}: {}!'.format(message_id, exc))
        else:
            logger_console.info('Message id is not set!')

    def update_message(token, message_id=None, text=None, sender=None, thread=None):
        """
        URL: http://localhost:8000/messages/<id>/
        Method: PUT
        """

        if message_id and text and sender and thread:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'id': message_id, 'text': text, 'sender': sender, 'thread': thread})

                session = requests.Session()
                request = session.put(API_URL_MESSAGES_GET_PUT_DELETE.format(message_id), headers=headers, data=data)
                logger_console.info('Update message with id {}: {}!\n'
                                    'Status code = {}!'.format(message_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error updating message with id {}: {}!'.format(message_id, exc))
        else:
            logger_console.info('Message id or participants or sender or thread are not set!')

    @staticmethod
    def delete_message(token, message_id=None):
        """
        URL: http://localhost:8000/messages/<id>/
        Method: Delete
        """

        if message_id:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'id': message_id})

                session = requests.Session()
                request = session.delete(API_URL_MESSAGES_GET_PUT_DELETE.format(message_id), headers=headers, data=data)
                logger_console.info('Message with id {} was deleted! '
                                    'Status code = {}!'.format(message_id, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error deleting message with id {}: {}!'.format(message_id, exc))
        else:
            logger_console.info('Message id is not set!')

    @staticmethod
    def create_message_for_thread(token, thread_id=None, text=None, sender=None):
        """
        URL: http://localhost:8000/threads/<thread_id>/messages/
        Method: POST
        """

        if thread_id and text and sender:
            try:
                headers = APIRequests.create_headers(token)
                data = json.dumps({'text': text, 'sender': sender})

                session = requests.Session()
                request = session.post(API_URL_THREADS_GET_ADD_MESSAGE.format(thread_id), headers=headers, data=data)
                logger_console.info('Create message for thread with id {}: {}!\n'
                                    'Status code = {}!'.format(thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error creating message for thread with id {}: {}!'.format(thread_id, exc))
        else:
            logger_console.info('Message can\'t be created without text, sender and thread id!')

    @staticmethod
    def get_all_messages_for_thread(token, thread_id=None):
        """
        URL: http://localhost:8000/threads/<thread_id>/messages/
        Method: GET
        """

        if thread_id:
            try:
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.get(API_URL_THREADS_GET_ADD_MESSAGE.format(thread_id), headers=headers)
                logger_console.info('Get messages for thread with id {}: {}!\n'
                                    'Status code = {}!'.format(thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error getting messages for thread with id {}: {}!'.format(thread_id, exc))
        else:
            logger_console.info('Thread id is not set!')

    @staticmethod
    def add_user_to_thread(token, thread_id=None, user_id=None):
        """
        URL: http://localhost:8000/threads/<thread_id>/users/<user_id>/
        Method: POST
        """

        if thread_id and user_id:
            try:
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.post(API_URL_THREADS_ADD_DELETE_USER.format(thread_id, user_id), headers=headers)
                logger_console.info('Add user with id {} to thread with id {}: {}!\n'
                                    'Status code = {}!'.format(user_id, thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error adding user with id {} to thread with id {}: '
                                    '{}!'.format(user_id, thread_id, exc))
        else:
            logger_console.info('Thread id or user id are not set!')

    @staticmethod
    def delete_user_from_thread(token, thread_id=None, user_id=None):
        """
        URL: http://localhost:8000/threads/<thread_id>/users/<user_id>/
        Method: DELETE
        """

        if thread_id and user_id:
            try:
                headers = APIRequests.create_headers(token)

                session = requests.Session()
                request = session.delete(API_URL_THREADS_ADD_DELETE_USER.format(thread_id, user_id), headers=headers)
                logger_console.info('Delete user with id {} from thread with id {}: {}!\n'
                                    'Status code = {}!'.format(user_id, thread_id, request.text, request.status_code))
                session.close()
            except Exception as exc:
                logger_console.info('Error deleting user with id {} from thread with id {}: '
                                    '{}!'.format(user_id, thread_id, exc))
        else:
            logger_console.info('Thread id or user id are not set!')

