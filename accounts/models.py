from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    USER_TYPE_ADMIN = 1
    USER_TYPE_DRIVER = 2

    USER_TYPE_CHOICES = (
        (USER_TYPE_ADMIN, 'Admin'),
        (USER_TYPE_DRIVER, 'Driver'),
    )

    user_type = models.IntegerField(choices=USER_TYPE_CHOICES, default=USER_TYPE_ADMIN)


