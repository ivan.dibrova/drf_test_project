import logging

from django.core.management.base import BaseCommand

from accounts.models import CustomUser
from accounts.api_requests import APIRequests

logger_console = logging.getLogger('console')


class Command(BaseCommand):
    """
    Testing API requests
    """

    def handle(self, *args, **kwargs):
        user = CustomUser.objects.get(id=1)
        access_token = APIRequests.get_user_access_token(user)
        logger_console.info('Access token for user {}: {}'.format(user, access_token))

        # APIRequests.get_user_threads(access_token, user_id='1')

        # APIRequests.create_message_for_thread(access_token, thread_id=7, text='Message_74', sender=19)
        # APIRequests.get_all_messages_for_thread(access_token, thread_id=7)

        # APIRequests.add_user_to_thread(access_token, thread_id=34, user_id=17)
        # APIRequests.delete_user_from_thread(access_token, thread_id=34, user_id=16)


        # APIRequests.create_user('driver2174', 'asd12345', access_token, user_type=CustomUser.USER_TYPE_DRIVER)
        # APIRequests.get_all_users(access_token)
        # APIRequests.get_one_user(access_token, username='driver_1')
        # APIRequests.get_one_user(access_token, user_id=1)
        # APIRequests.update_user(access_token, user_id='45', username='driver_91', password='asd12345',
        #                         user_type=CustomUser.USER_TYPE_DRIVER)
        # APIRequests.delete_user(access_token, user_id='42')
        # APIRequests.delete_user(access_token, username='driver_41')


        # APIRequests.create_thread(access_token, participants=[1,15,39])
        # APIRequests.get_all_threads(access_token)
        # APIRequests.get_one_thread(access_token, thread_id=2)
        # APIRequests.update_thread(access_token, thread_id=2, participants=[18,19,45])
        # APIRequests.delete_thread(access_token, thread_id=5)


        # APIRequests.create_message(access_token, text='Message_1', sender=1, thread=7)
        # APIRequests.get_all_messages(access_token)
        # APIRequests.get_one_message(access_token, message_id=13)
        # APIRequests.update_message(access_token, message_id=13, text="Message_7_3", sender=31, thread=7)
        # APIRequests.delete_message(access_token, message_id=14)




