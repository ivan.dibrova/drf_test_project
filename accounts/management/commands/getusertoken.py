import logging

from django.contrib.auth import authenticate
from django.core.management.base import BaseCommand

from rest_framework_simplejwt.tokens import AccessToken

logger_console = logging.getLogger('console')


class Command(BaseCommand):
    """
    Get access token for existing user
    """

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='User name (login)')
        parser.add_argument('-p', '--password', type=str, help='User password')

    def handle(self, *args, **kwargs):
        username = kwargs['username']
        password = kwargs['password']
        access_token = 'Token not created!'

        if username and password:
            try:
                user = authenticate(username=username, password=password)
                access_token = str(AccessToken.for_user(user))
            except Exception as exc:
                logger_console.info('Error creating token: {}!'.format(exc))
        else:
            logger_console.info('Error: Not enough parameters!')

        return access_token

