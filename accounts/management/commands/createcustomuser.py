import logging

from django.core.management.base import BaseCommand
from django.core.validators import validate_email
from django.contrib.auth.hashers import make_password

from rest_framework_simplejwt.tokens import AccessToken

from accounts.models import CustomUser

logger_console = logging.getLogger('console')


class Command(BaseCommand):
    """
    Create CustomUser and obtain access token
    """

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='Set user name (login) (required)')
        parser.add_argument('-p', '--password', type=str, help='Set user password (required)')
        parser.add_argument('-e', '--email', type=str, help='Set user email (optional)')
        parser.add_argument('-f', '--firstname', type=str, help='Set user (optional)')
        parser.add_argument('-l', '--lastname', type=str, help='Set user (optional)')
        parser.add_argument('-t', '--usertype', type=str, help='Set user type (optional)')

    def handle(self, *args, **kwargs):
        username = kwargs['username']
        password = kwargs['password']
        email = kwargs['email']
        firstname = kwargs['firstname']
        lastname = kwargs['lastname']
        user_type = kwargs['usertype']
        access_token = 'Token not created!'

        if username and password:
            try:
                try:
                    validate_email(email)
                except Exception:
                    email = ''

                firstname = firstname if firstname else ''
                lastname = lastname if lastname else ''

                try:
                    user_type = CustomUser.USER_TYPE_ADMIN \
                        if int(user_type) == CustomUser.USER_TYPE_ADMIN \
                        else CustomUser.USER_TYPE_DRIVER
                except Exception:
                    user_type = CustomUser.USER_TYPE_ADMIN

                is_staff = True if user_type == CustomUser.USER_TYPE_ADMIN else False
                is_superuser = True if user_type == CustomUser.USER_TYPE_ADMIN else False

                user = CustomUser.objects.create(username=username,
                                          password=make_password(password),
                                          email=email,
                                          first_name=firstname,
                                          last_name=lastname,
                                          user_type=user_type,
                                          is_staff=is_staff,
                                          is_superuser=is_superuser)

                logger_console.info('User {} created successfully!'.format(username))

                access_token = str(AccessToken.for_user(user))
                logger_console.info('Token for User {} obtained successfully!'.format(username))

            except Exception as exc:
                logger_console.info('User not created! Error: {}!'.format(exc))
        else:
            logger_console.info('User not created! Error: Not enough parameters!')

        return access_token

