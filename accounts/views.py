from rest_framework import generics
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from accounts.models import CustomUser
from accounts.serializers import CustomUserSerializer
from dialogs.models import Thread
from dialogs.serializers import ThreadSerializer


class CustomUserList(generics.ListCreateAPIView):
    """
    Create user and get all users.
    URL: http://localhost:8000/users/
    Allowed methods: GET, POST
    """

    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class CustomUserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Get user, update user, delete user.
    URL: http://localhost:8000/users/<id>/
    Allowed methods: GET, PUT, DELETE
    """

    queryset = CustomUser.objects.all()
    serializer_class = CustomUserSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class CustomUserThreadList(APIView):
    """
    Get threads for user.
    URL: http://localhost:8000/users/<user_id>/threads/
    Allowed methods: GET
    """

    def get(self, request, pk):
        threads = Thread.objects.filter(participants__id=pk)
        serializer = ThreadSerializer(threads, many=True)
        return Response(serializer.data)

