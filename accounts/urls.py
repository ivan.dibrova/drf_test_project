from django.urls import path

from accounts.views import CustomUserList, CustomUserDetail, CustomUserThreadList


urlpatterns = [
    # GET, POST
    path('users/', CustomUserList.as_view()),
    # GET, PUT, DELETE
    path('users/<int:pk>/', CustomUserDetail.as_view()),

    # GET
    path('users/<int:pk>/threads/', CustomUserThreadList.as_view()),
]


